angular.module("weatherApp").controller('homeController', ['$scope', '$location', 'cityService', function($scope, $location, cityService) {

    $scope.city = cityService.city;

    $scope.$watch("city", function() {
        cityService.city = $scope.city;
    });

    $scope.submit = function() {
        if ($scope.city) {
            $location.path("/forecast");
        }
    };

}]);

angular.module("weatherApp").controller('forecastController', ['$scope',
                                             '$routeParams',
                                             '$location',
                                             'cityService',
                                             'weatherService', function($scope, $routeParams, $location, cityService, weatherService) {
    
    $scope.city = cityService.city;

    var cnt = $routeParams.cnt || "2";
    $scope.cnt = cnt;

    $scope.weatherResult = weatherService.getWeather($scope.city, $scope.cnt);

    $scope.convertToCelsius = function(degK) {
        return degK - 273.15;
    }

    $scope.convertToDate = function(date) {
        return new Date(date * 1000);
    }

}]);
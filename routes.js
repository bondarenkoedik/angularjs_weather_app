angular.module("weatherApp").config(['$routeProvider', function($routeProvider) {

    $routeProvider

    .when('/', {
        templateUrl: 'pages/home.html',
        controller: 'homeController'
    })

    .when('/forecast/:cnt?', {
        templateUrl: 'pages/forecast.html',
        controller: 'forecastController'
    })

}]);
angular.module("weatherApp").service('cityService', function() {
    this.city = "New York, NY";

});

angular.module("weatherApp").service('weatherService', ['$resource', function($resource) {

    this.getWeather = function(city, cnt) {

        var weatherAPI = $resource("http://api.openweathermap.org/data/2.5/forecast/daily?APPID=15520383a98736820bdb0b529da77558",
            { callback: "JSON_CALLBACK" }, { get: { method: "JSONP" } }
        );

        return weatherAPI.get({ q: city, cnt: cnt });

    };
}]);
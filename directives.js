// weatherApp.directive('weather', function() {
angular.module("weatherApp").directive('weather', function() {
    return {
        restrict: "E",
        templateUrl: 'directives/weather.html',
        replate: true,
        scope: {
            weatherDay: "=",
            convertToStandard: "&",
            convertToDate: "&",
            dateFormat: "@"
        },
        transclude: true,
    }
});